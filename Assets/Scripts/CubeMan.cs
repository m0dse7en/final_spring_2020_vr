﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class CubeMan : MonoBehaviourPunCallbacks, IOnEventCallback, IPunObservable
{
	[Tooltip("How fast to move the cube")]
	public float speed = 2f;

	private Renderer rend;
	private float red = 0;

	// Start is called before the first frame update
	void Start()
	{
		rend = GetComponent<Renderer>();
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			// make sure to take ownership if we don't have it already before manipulating it locally
			if (!photonView.IsMine)
			{
				photonView.TransferOwnership(PhotonNetwork.LocalPlayer);
			}

			// move the cube locally
			transform.Translate(-Time.deltaTime * speed, 0, 0);

		}


		if (Input.GetKey(KeyCode.RightArrow))
		{
			// take ownership if we don't have it
			if (!photonView.IsMine)
			{
				photonView.TransferOwnership(PhotonNetwork.LocalPlayer);
			}

			// move the cube locally
			transform.Translate(Time.deltaTime * speed, 0, 0);

		}

		// if we own the cube, cycle its color
		if (photonView.IsMine)
		{
			red = Mathf.Sin(Time.time) / 2 + .5f;
			rend.material.color = new Color(red, 0, 0);

		}

		// Change the color using an RPC
		if (Input.GetKeyDown(KeyCode.R))
		{
			photonView.RPC(nameof(ChangeColorRPC), RpcTarget.AllBuffered, 0f, 0f, 1f);
		}

		// Change the color using Events:
		if (Input.GetKeyDown(KeyCode.C))
		{
			float[] color = { 0, 0, 1f };
			PhotonNetwork.RaiseEvent(
				(byte)PhotonMan.MessageTypes.CubeColor, 
				(object)color, 
				new RaiseEventOptions { Receivers = ReceiverGroup.All }, 
				new SendOptions());
		}
	}

	[PunRPC]
	public void ChangeColorRPC(float r, float g, float b)
	{
		GetComponent<Renderer>().material.color = new Color(r, g, b);
		Debug.Log("Changed color with RPC");
	}

	public void ChangeColorEvent(float r, float g, float b)
	{
		GetComponent<Renderer>().material.color = new Color(r, g, b);
		Debug.Log("Changed color with Event");
	}

	public void OnEvent(EventData photonEvent)
	{
		if (photonEvent.Code == (byte)PhotonMan.MessageTypes.CubeColor)
		{
			float[] color = (float[])photonEvent.CustomData;
			ChangeColorEvent(color[0], color[1], color[2]);
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			stream.SendNext(red);
		}
		else
		{
			red = (float)stream.ReceiveNext();
			rend.material.color = new Color(red, 0, 0);
		}
	}
}
