﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;

public class PhotonMan : MonoBehaviourPunCallbacks
{
	/// <summary>
	/// All the different event types that we use
	/// </summary>
	public enum MessageTypes
	{
		CubeColor
	}

	public GameObject cubePrefab;
	public GameObject playerPrefab;
	public Rig rig;

	// Start is called before the first frame update
	void Start()
	{
		// Changes the updates/s of the photon network. Default is 20
		//PhotonNetwork.SendRate = 2;

		// Connect to photon
		PhotonNetwork.ConnectUsingSettings();

		StartCoroutine(Reconnect());
	}

	/// <summary>
	/// Tries to reconnect every once in a while if we happen to disconnect.
	/// This is relatively common on Quest, because the headset goes to sleep.
	/// </summary>
	IEnumerator Reconnect()
	{
		yield return new WaitForSeconds(5f);
		if (!PhotonNetwork.IsConnected)
		{
			PhotonNetwork.ConnectUsingSettings();
		}
	}

	public override void OnConnectedToMaster()
	{
		Debug.Log("Connect to master");

		PhotonNetwork.JoinOrCreateRoom("room", new RoomOptions(), TypedLobby.Default);
	}

	public override void OnJoinedRoom()
	{
		Debug.Log("Joined Room");
		Debug.Log("Players in room: " + PhotonNetwork.CurrentRoom.PlayerCount);

		// Instantiate the local avatar
		Avatar avatar = PhotonNetwork.Instantiate(playerPrefab.name, Vector3.zero, Quaternion.identity).GetComponent<Avatar>();
		avatar.rig = rig;
	}

	// Update is called once per frame
	void Update()
	{
		// example of instantiating an object on the network
		if (Input.GetKeyDown(KeyCode.I))
		{
			PhotonNetwork.Instantiate(cubePrefab.name, Vector3.zero, Quaternion.identity);
		}
	}
}
