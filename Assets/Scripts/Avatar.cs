﻿using Photon.Pun;
using UnityEngine;

public class Avatar : MonoBehaviourPunCallbacks, IPunObservable
{
	// The head/hands of the VR-controlled local rig
	public Rig rig;

	// The head/hands of the avatar
	public Transform head;
	public Transform leftHand;
	public Transform rightHand;

	// Variables containing the most recently received position/rotation from the network
	// (used for interpolation)
	private Vector3 networkHeadPos;
	private Quaternion networkHeadRot;
	private Vector3 networkLeftHandPos;
	private Vector3 networkRightHandPos;

	private void Start()
	{
		// Disable the head on the local avatar
		if (photonView.IsMine)
		{
			head.gameObject.SetActive(false);
		}
	}


	// Update is called once per frame
	void Update()
	{
		// if this is the local avatar
		if (photonView.IsMine)
		{
			// move the local avatar to match the local rig
			leftHand.position = rig.leftHand.position;
			rightHand.position = rig.rightHand.position;
		}
		// if this is a remote avatar (represents another player in our scene)
		else
		{
			// move the avatar components to match the network position smoothly
			head.position = Vector3.Lerp(head.position, networkHeadPos, .05f);
			head.rotation = Quaternion.Slerp(head.rotation, networkHeadRot, .05f);
			leftHand.position = Vector3.Lerp(leftHand.position, networkLeftHandPos, .05f);
			rightHand.position = Vector3.Lerp(rightHand.position, networkRightHandPos, .05f);
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			// Send the position of our local camera rig
			stream.SendNext(rig.head.position);
			stream.SendNext(rig.head.rotation);
			stream.SendNext(rig.leftHand.position);
			stream.SendNext(rig.rightHand.position);
		}
		else
		{
			// Receive avatar data from the network
			networkHeadPos = (Vector3)stream.ReceiveNext();
			networkHeadRot = (Quaternion)stream.ReceiveNext();
			networkLeftHandPos = (Vector3)stream.ReceiveNext();
			networkRightHandPos = (Vector3)stream.ReceiveNext();
		}
	}
}
