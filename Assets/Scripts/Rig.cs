﻿using UnityEngine;

/// <summary>
/// Stores the local VR-controlled head/hand objects
/// </summary>
public class Rig : MonoBehaviour
{
	public Transform head;
	public Transform leftHand;
	public Transform rightHand;
}
